package fr.badcookie20.utils;

import fr.badcookie20.utils.math.polynomial.Polynomial;
import fr.badcookie20.utils.math.polynomial.SecondDegreePolynomial;
import fr.badcookie20.utils.misc.Timer;

public class Main {

    public static void main(String[] args) {
        Timer t = new Timer();
        start();
        t.displayDiff();
    }

    public static void start() {
        Polynomial p = new SecondDegreePolynomial(-5, -70, -245);
    }

}

