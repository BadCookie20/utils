package fr.badcookie20.utils.io;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe offrant des méthodes pour l'IO
 * @author BadCookie20
 */
public class IOUtils {

    /**
     * Permet de récupérer toutes les lignes d'un fichier
     * @param file le fichier duquel on veut récupérer les lignes
     * @return les lignes du fichier spécifié
     * @throws IOException si une exception est levée pendant la lecture des lignes
     */
    public static List<String> getLines(File file) throws IOException {
        return getLines(getBufferedReader(file));
    }

    /**
     * Permet de récupérer les lignes d'un BufferedReader
     * @param bufferedReader le BufferedReader duquel on veut récupérer les lignes
     * @return les lignes contenues dans le BufferedReader spécifié
     * @throws IOException si une exception est levée pendant la lecture des lignes
     */
    public static List<String> getLines(BufferedReader bufferedReader) throws IOException {
        List<String> lines = new ArrayList<>();

        String readLine = "";
        while ((readLine = bufferedReader.readLine()) != null) {
            if(readLine.isEmpty()) continue;
            lines.add(readLine);
        }

        return lines;
    }

    /**
     * Permet de récupérer le BufferedReader d'un fichier
     * @param file le fichier duquel on veut récupérer le BufferedReader
     * @return le BufferedReader du fichier
     * @throws FileNotFoundException si le fichier spécifié n'existe pas
     */
    public static BufferedReader getBufferedReader(File file) throws FileNotFoundException {
        DataInputStream in = new DataInputStream(new FileInputStream(file));
        return new BufferedReader(new InputStreamReader(in));
    }

    /**
     * Permet de récupérer un fichier par son URL
     * @param url l'URL du fichier
     * @return le fichier correspondant à l'URL (même s'il n'existe pas)
     */
    public static File getFileByURI(URL url) {
        return new File(url.getPath());
    }

}
