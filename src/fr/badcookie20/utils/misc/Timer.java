package fr.badcookie20.utils.misc;

/**
 * Class permettant de calculer un écart de temps entre deux moments dans l'exécution d'un programme
 * @author BadCookie20
 */
public class Timer {

    private long timemillis;

    /**
     * Constructeur de la classe. Définit le temps 0
     */
    public Timer() {
        timemillis = System.currentTimeMillis();
    }

    /**
     * Permet de récupérer la différence par rapport au temps 0 établi lors de l'instanciation ou par l'exécution de Timer.updateBase
     * @return la différence en millisecondes entre le temps 0 et le moment de l'exécution
     */
    public long getDiff() {
        return System.currentTimeMillis() - timemillis;
    }

    /**
     * Permet de définir le temps 0 au temps actuel
     */
    public void updateBase() {
        this.timemillis = System.currentTimeMillis();
    }

    public void displayDiff() {
        System.out.println(getDiff());
    }
}
