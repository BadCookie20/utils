package fr.badcookie20.utils.misc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe contenant des méthodes en relation avec les Arrays
 * @author BadCookie20
 */
public class ArrayUtils {

    /**
     * Permet de convertir l'Array spécifié en une ArrayList générique en le type des objets spécifiés
     * @param objects les objets à intégrer dans l'Array
     * @return une ArrayList contenant les valeurs spécifiées, ou une ArrayList vide si les objets spécifiés sont
     * <code>null</code> ou si l'array spécifié est vide
     */
    public static List<?> toList(Object... objects) {
        if(objects == null || objects.length == 0) return new ArrayList<>();
        List<Object> list = new ArrayList<>();
        Collections.addAll(list, objects);
        return list;
    }

}
