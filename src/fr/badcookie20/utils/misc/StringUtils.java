package fr.badcookie20.utils.misc;

import java.text.Normalizer;

/**
 * Classe utilitaire de Strings
 * @author BadCookie20
 */
public class StringUtils {

    public static String removeAccents(String s) {
        System.out.println(s);
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }

}
