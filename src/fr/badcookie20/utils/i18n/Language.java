package fr.badcookie20.utils.i18n;

import fr.badcookie20.utils.io.IOUtils;
import fr.badcookie20.utils.math.MathUtils;
import fr.badcookie20.utils.misc.StringUtils;

import java.io.IOException;
import java.util.List;

/**
 * Classe offrant divers outils pour l'i18n
 * @author BadCookie20
 */
public enum Language {

    FRENCH("lang_fr.txt", "fr", "français", "french"),
    ENGLISH("lang_en.txt", "en", "english", "english"),
    SPANISH("lang_es.txt", "es", "español", "spanish");

    private String filePath;
    private String abr;
    private String languageName;
    private String englishName;

    private List<String> lines;

    /**
     * Constructeur d'une langue
     * @param filePath l'URI du fichier de lange à partir de cette classe
     * @param abr l'abréviation de la langue (ex: fr, es...)
     * @param languageName le nom complet de la langue dans sa langue
     * @param englishName le nom complet de la langue en anglais
     */
    Language(String filePath, String abr, String languageName, String englishName) {
        this.filePath = filePath;
        this.abr = abr;
        this.languageName = languageName;
        this.englishName = englishName;

        try {
            lines = IOUtils.getLines(IOUtils.getFileByURI(this.getClass().getResource(this.filePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de récupérer l'URI du fichier de langue
     * @return l'URI du fichier
     */
    public String getFilePath() {
        return this.filePath;
    }

    /**
     * Permet de récupérer l'intégralité des lignes du fichier de langue
     * @return les lignes du fichier
     */
    public List<String> getLines() {
        return this.lines;
    }

    /**
     * Permet de récupérer l'abréviation de la langue
     * @return l'abréviation de la langue
     */
    public String getAbr() {
        return this.abr;
    }

    /**
     * Permet de savoir si le mot spécifié est contenu dans le fichier de langue de la langue choisie
     * @param word le mot à tester
     * @return <code>true</code> si il y est, <code>false</code> si non
     */
    public boolean isWordInLanguage(String word) {
        word = StringUtils.removeAccents(word);
        for(String line : lines) {
            if(line.equalsIgnoreCase(word)) return true;
        }

        return false;
    }

    /**
     * Permet de récupérer la langue du mot spécifié. Comme il se peut que le même mot existe dans différentes langues,
     * la langue donnée ne sera pas forcément la langue correcte. La première langue testée est le {@link #FRENCH}, puis
     * le {@link #ENGLISH} et la dernière le {@link #SPANISH}. La méthode retourne <code>null</code> si le mot ne se trouve
     * dans aucune langue
     * @param word le mot à tester
     * @return la langue du mot, ou <code>null</code> s'il ne se trouve dans aucune langue
     */
    private static Language getLanguage(String word) {
        for(Language l : Language.values()) {
            if(l.isWordInLanguage(word)) return l;
        }

        return null;
    }

    /**
     * Permet de récupérer la langue des mots spécifiés (utilise la méthode {@link #getLanguage(String...)} pour déterminer
     * la langue de chaque mot individuellement). Plus le nombre de mots testés est grand, plus il y aura de chances que
     * la langue retournée soit la correcte (il se peut en effet que plusieurs langues possèdent le même mot). Si aucune
     * langue ne possède le mot spécifié, la méthode retourne <code>null</code>. Si deux langues (ou plus) arrivent à
     * égalité, le {@link #FRENCH} est retourné en premier, puis le {@link #ENGLISH} puis le {@link #SPANISH}.
     * @param words les mots à tester.
     * @return la langue des mots, ou <code>null</code> si les mots ne sont contenus dans aucune langue
     */
    public static Language getLanguage(String... words) {
        if(words.length == 1) return getLanguage(words[0]);

        int french = 0;
        int spanish = 0;
        int english = 0;

        for(String word : words) {
            Language l = getLanguage(word);
            if(l == null) continue;
            if(l == Language.ENGLISH) english++;
            if(l == Language.FRENCH) french++;
            if(l == Language.SPANISH) spanish++;
        }

        int greater = MathUtils.getGreater(french, spanish, english);

        if(greater == 0) return null;

        if(greater == french) return Language.FRENCH;
        if(greater == english) return Language.ENGLISH;
        if(greater == spanish) return Language.SPANISH;

        return null;
    }

    @Override
    public String toString() {
        return this.englishName;
    }
}
