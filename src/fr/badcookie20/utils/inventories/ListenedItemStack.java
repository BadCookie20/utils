package fr.badcookie20.utils.inventories;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import org.bukkit.inventory.ItemStack;

/**
 * Cette classe représente l'ItemStack avec un listener
 * @author BadCookie20
 */
public abstract class ListenedItemStack extends ItemStack {

    /**
     * Constructeur de l'item. S'enregistre tout seul auprès du listener
     * @param stack l'item correspondant
     * @throws IllegalArgumentException si l'item est déjà enregistré (et donc peut causer des conflits)
     */
    public ListenedItemStack(ItemStack stack) throws IllegalArgumentException {
        super(stack);

        DefaultListener.getInstance().addItem(this);
    }

    /**
     * Méthode appelée lors du click de cet item
     * @param c le ClickContext correspondant
     */
    public abstract void onClick(ClickContext c);

}
