package fr.badcookie20.utils.inventories;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Ce listener est le listener qui permet d'écouter tout ce qui a à voir avec les items possedant un listener. Il doit
 * être instancié AVANT son utilisation
 * @author BadCookie20
 */
public class DefaultListener implements Listener {

    private static DefaultListener instance;

    private List<ListenedItemStack> items;

    public DefaultListener() {
        items = new ArrayList<>();
        instance = this;
    }

    public static DefaultListener getInstance() {
        return instance;
    }

    public void addItem(ListenedItemStack item) throws IllegalArgumentException {
        if(containsSimilar(item)) throw new IllegalArgumentException("there is already a listened item with the same data");
        items.add(item);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if(!containsSimilar(e.getCurrentItem())) return;

        for(ListenedItemStack item : items) {
            if(areSimilar(item, e.getCurrentItem())) {
                ClickContext context = new ClickContext(e.getInventory(), e.getWhoClicked(), e.getClick());
                item.onClick(context);
                return;
            }
        }
    }

    private boolean containsSimilar(ItemStack item1) {
        for(ListenedItemStack item2 : this.items) {
            if(areSimilar(item2, item1)) return true;
        }

        return false;
    }

    private boolean areSimilar(ListenedItemStack item1, ItemStack item2) {
        if(item1 == null && item2 == null) return true;
        if(item1.equals(item2)) return true;

        if(item1.getType() != item2.getType()) return false;
        if(item1.getAmount() != item2.getAmount()) return false;
        if(item1.hasItemMeta() && !item2.hasItemMeta()) return false;

        return item1.getItemMeta().equals(item2.getItemMeta());
    }

}
