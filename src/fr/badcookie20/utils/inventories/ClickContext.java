package fr.badcookie20.utils.inventories;

/*
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 */

import org.bukkit.entity.Entity;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;

/**
 * Cette classe est utilisée dans le contexte des items possedant un listener
 * @author BadCookie20
 */
public class ClickContext {

    private Inventory inventory;
    private Entity clicker;
    private ClickType type;

    /**
     * Permet de déclarer un ClickContext
     * @param inventory l'inventaire dans lequel est cliqué l'item
     * @param clicker celui qui a cliqué l'item
     * @param type le type de click effectué (click droit, gauche...)
     */
    public ClickContext(Inventory inventory, Entity clicker, ClickType type) {
        this.inventory = inventory;
        this.clicker = clicker;
        this.type = type;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public Entity getClicker() {
        return clicker;
    }

    public ClickType getType() {
        return type;
    }
}
