package fr.badcookie20.utils.stats.lists;

import fr.badcookie20.utils.misc.ArrayUtils;
import fr.badcookie20.utils.stats.Couple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Classe abstraite commune à toutes les listes statistiques. Elle hérite de {@link StatsList}
 * @author BadCookie20
 */
public abstract class AbstractStatsList implements StatsList {

    protected List<Double> values;

    public AbstractStatsList(Integer... values) {
        this.values = (List<Double>) ArrayUtils.toList(values);
    }

    public AbstractStatsList() {
        this.values = new ArrayList<>();
    }

    @Override
    public void addValue(double value) {
        values.add(value);
    }

    @Override
    public void emptyList() {
        values = new ArrayList<>();
    }

    @Override
    public void replaceList(StatsList list) {
        values = Arrays.asList(list.toValueArray());
    }

    @Override
    public int getSize() {
        return values.size();
    }

    @Override
    public Double[] toValueArray() {
        return (Double[]) values.toArray();
    }

    @Override
    public Couple[] toCoupleArray() {
        Couple[] couples = new Couple[values.size()];
        int i = 0;
        for(double value : this.values) {
            couples[i] = new Couple(i, value);
            i++;
        }

        return couples;
    }
}
