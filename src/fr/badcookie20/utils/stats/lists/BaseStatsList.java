package fr.badcookie20.utils.stats.lists;

import fr.badcookie20.utils.stats.Couple;

/**
 * Interface de base du système d'utilitaires de statistiques. Toutes les listes doivent implémenter cette interface. Elle contient les
 * méthodes de base pour correctement les utiliser
 * @author BadCookie20
 */
public interface BaseStatsList {

    /**
     * Permet d'ajouter une valeur à la liste. Elle est ajoutée en bout de liste. Si la liste est vide, elle
     * occupe à elle seule la liste
     * @param value la valeur à ajouter. Elle peut être négative
     */
    void addValue(double value);

    /**
     * Permet de récupérer la valeur au rang désiré. Si aucune valeur ne correspond au rang, une {@link ArrayIndexOutOfBoundsException} est
     * lancée. Si la liste est vide, une {@link ArrayIndexOutOfBoundsException} est lancée.
     * @param index Le rang de la valeur à laquelle on souhaite accéder
     * @return la valeur désirée
     */
    Couple getAt(int index);

    /**
     * Permet de récupérer la taille de la liste statistique. Si la liste est vide, elle est de 0
     * @return la taille de la liste
     */
    int getSize();

    /**
     * Permet de faire la somme de toutes les valeurs de la série statistique. Si la liste est vide, cette méthode retourne 0
     * @return la somme des valeurs de la liste
     */
    double sumUpValues();

    /**
     * Permet de convertir la liste statistique en un array de nombre décimaux. Si il n'y a aucune valeur dans la liste, l'array est vide
     * @return un array de doubles
     */
    Double[] toValueArray();

}
