package fr.badcookie20.utils.stats.lists;

import fr.badcookie20.utils.stats.Couple;

/**
 * Interface du système de statistiques permettant une utilisation plus poussée du système
 * @author BadCookie20
 */
public interface StatsList extends BaseStatsList {

    /**
     * Permet de vider la liste statistique.
     */
    void emptyList();

    /**
     * Permet de remplacer cette liste statistique par celle spécifiée
     * @param list la liste qui remplace celle-ci.
     */
    void replaceList(StatsList list);

    /**
     * Permet de ranger la liste statistique dans l'ordre croissant. Si il n'y a aucune ou une seule valeur dans la série
     * statistique, cette méthode n'exécute rien.
     */
    void sortIncreasingOrder();

    /**
     * Permet de ranger la liste statistique dans l'ordre décroissant. Si il n'y a aucune ou une seule valeur dans la série
     * statistique, cette méthode n'exécute rien.
     */
    void sortDecreasingOrder();

    /**
     * Permet de récupérer la médiane de la série statistique. Si il n'y a aucune valeur dans la série statistique, cette
     * méthode retourne <code>null</code>
     */
    Couple getMed();

    /**
     * Permet de récupérer la moyenne de la série statistique. Si il n'y a aucune valeur dans la série statistique, cette
     * méthode retourne 0
     * @return la moyenne de la série, ou 0 si il n'y a aucune valeur
     */
    double getMoy();

    /**
     * Permet de récupérer le premier quartile de la série statistique. Elle est calculée en divisant la taille de la liste
     * par 4, puis en retournant la valeur de rang taille_liste/4 arrondie à l'entier supérieur. Si il n'y a aucune valeur
     * dans la série statistique, cette méthode retourne <code>null</code>
     * @return le premier quartile de la série statistique.
     */
    Couple getQ1();

    /**
     * Permet de récupérer le troisième quartile de la série statistique. Elle est calculée en divisant la taille de la liste
     * par 4 puis en la multipliant par 3, puis en retournant la valeur de rang (taille_liste/4)*3 arrondie à l'entier supérieur.
     * Si il n'y a aucune valeur dans la série statistique, cette méthode retourne <code>null</code>
     * @return le troisième quartile de la série statistique.
     */
    Couple getQ3();

    /**
     * Permet de transformer la liste statistique en un array de Couples
     * @return un array de couples
     */
    Couple[] toCoupleArray();

}
