package fr.badcookie20.utils.stats.lists;

import fr.badcookie20.utils.stats.Couple;

import java.util.*;

/**
 * Classe du système de statistiques permettant une utilisation par défaut (médiane, moyenne...). Elle utilise le système français
 * pour trouver les quartiles et les médianes
 * @author BadCookie20
 */
public final class DefaultStatsList extends AbstractStatsList {

    public DefaultStatsList(Integer... values) {
        super(values);
    }

    @Override
    public void sortIncreasingOrder() {
        if(this.getSize() == 0 || this.getSize() == 1) return;
        Double[] values = new Double[this.values.size() - 1];
        values = this.values.toArray(values);
        Arrays.sort(values);
        this.values = Arrays.asList(values);
    }

    @Override
    public void sortDecreasingOrder() {
        if(this.getSize() == 0 || this.getSize() == 1) return;
        Double[] values = new Double[this.values.size() - 1];
        values = this.values.toArray(values);
        Arrays.sort(values);

        List<Double> newValues = Arrays.asList(values);

        Comparator comparator = Collections.reverseOrder();
        Collections.sort(newValues, comparator);

        this.values = newValues;
    }

    @Override
    public Couple getMed() {
        if(this.getSize() == 0) return null;
        if(this.getSize() == 1) return getAt(0);
        int size = this.getSize();
        int index = -1;
        if(size % 2 == 0) {
            index = ((size/2) + (size/2 + 1)) / 2;
        }else{
            index = ((size+1)/2);
        }
        return this.getAt(index);
    }

    @Override
    public double getMoy() {
        if(this.getSize() == 0) return 0;
        double total = this.sumUpValues();
        int size = this.getSize();
        return total/size;
    }

    @Override
    public Couple getQ1() {
        if(this.getSize() == 0) return null;
        if(this.getSize() == 1) return getAt(0);
        int size = this.getSize();
        double index = size/4;
        int finalIndex = (int) Math.ceil(index);
        return getAt(finalIndex);
    }

    @Override
    public Couple getQ3() {
        if(this.getSize() == 0) return null;
        if(this.getSize() == 1) return getAt(0);
        int size = this.getSize();
        double index = (size/4) * 3;
        int finalIndex = (int) Math.ceil(index);
        return getAt(finalIndex);
    }

    @Override
    public double sumUpValues() {
        if(this.getSize() == 0) return 0;
        int total = 0;
        for(double value : this.values) {
            total+=value;
        }

        return total;
    }

    @Override
    public Couple getAt(int index) {
        if(this.getSize() == 0) throw new ArrayIndexOutOfBoundsException();
        return new Couple(index, this.values.get(index));
    }

    /**
     * Permet d'afficher les valeurs de la table de la forme : index: value. Si la liste est vide, elle affiche "empty list"
     */
    public void displayValues() {
        if(this.getSize() == 0) {
            System.out.println("empty list");
            return;
        }
        for(Couple c : toCoupleArray()) {
            System.out.println(c.getIndex() + ": " + c.getValue());
        }
    }
}
