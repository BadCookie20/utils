package fr.badcookie20.utils.stats;

/**
 * Objet permettant de rentrer le rang de la valeur statistique ainsi que sa valeur
 * @author BadCookie20
 */
public class Couple {

    private int index;
    private double value;

    /**
     * Constructeur du couple.
     * @param index le rang de la valeur dans la liste statistique
     * @param value la valeur du couple
     */
    public Couple(int index, double value) {
        this.index = index;
        this.value = value;
    }

    /**
     * Permet de récupérer le rang de la valeur
     * @return le rang de la valeur
     */
    public int getIndex() {
        return index;
    }

    /**
     * Permet de récupérer la valeur du couple
     * @return la valeur du couple
     */
    public double getValue() {
        return value;
    }

    /**
     * Permet de définir la valeur du couple
     * @param value la nouvelle valeur du couple
     */
    public void setValue(int value) {
        this.value = value;
    }
}
