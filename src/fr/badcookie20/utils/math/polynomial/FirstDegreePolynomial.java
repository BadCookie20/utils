package fr.badcookie20.utils.math.polynomial;

/**
 * Cette classe correspond à un polynôme de degré 1
 * @author BadCookie20
 */
public class FirstDegreePolynomial implements Polynomial {

    private static final String REGEX = "\\+";
    private static final String UNKNOWN = "x";

    private double slope;
    private double yIntercept;

    /**
     * Constructeur du polynome.
     * @param slope le coefficient directeur du polynôme
     * @param yIntercept l'ordonnée à l'origine du polynome
     */
    public FirstDegreePolynomial(double slope, double yIntercept) {
        this.slope = slope;
        this.yIntercept = yIntercept;
    }

    @Override
    public double calculateImage(double image) {
        return slope*image + yIntercept;
    }

    @Override
    public double[] findImages(double value) {
        double tempB = value - this.yIntercept;
        double tempA = this.slope;

        tempB /= tempA;
        return new double[] {tempB};
    }

    /**
     * Permet de récupérer l'ordonnée à l'origine du polynôme
     * @return l'ordonnée à l'origine du polynôme, ou 0 si non spécifié
     */
    public double getYIntercept() {
        return this.yIntercept;
    }

    /**
     * Permet de récupérer le coefficient directeur du polynôme
     * @return le coefficient directeur du polynôme
     */
    public double getSlope() {
        return this.slope;
    }


}
