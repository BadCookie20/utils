package fr.badcookie20.utils.math.polynomial;

public class SecondDegreePolynomial implements Polynomial{

    private static final String REGEX = "\\+";
    private static final String POW = "^2";
    private static final String UNKNOWN = "x";

    private double a;
    private double b;
    private double c;

    private double discriminant;
    private double doubleRoot;

    public SecondDegreePolynomial(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;

        initMetadata();
    }

    private void initMetadata() {
        this.discriminant = Math.pow(this.b, 2) - 4*this.a*this.c;
        this.doubleRoot = -(this.b/ (2*this.a) );
    }

    @Override
    public double calculateImage(double image) {
        return a*(Math.pow(image, 2)) + b*image + c;
    }

    @Override
    public double[] findImages(double value) {
        if(value != 0) return new SecondDegreePolynomial(this.a, this.b, this.c - value).findImages(0);

        if(discriminant <0) return null;
        if(discriminant == 0) return new double[] {this.doubleRoot};

        double root1, root2;
        root1 = ( (-this.b - Math.sqrt(this.discriminant)) /  (2 * this.a));
        root2 = ( (-this.b + Math.sqrt(this.discriminant)) /  (2 * this.a));

        return new double[] {root1, root2};
    }
}
