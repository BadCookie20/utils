package fr.badcookie20.utils.math.polynomial;

/**
 * Interface de base de tous les polynômes
 * @author BadCookie20
 */
public interface Polynomial {

    /**
     * Permet de calculer l'image du nombre spécifié par le polynôme.
     * @param image l'image à calculer
     * @return l'image du nombre
     */
    double calculateImage(double image);

    /**
     * Permet de récupérer la ou les images du polynôme du nombre spécifié. Si il n'y a aucune solution réelle, la méthode
     * retourne null. Si il n'y a qu'un seul nombre solution, la méthode retourne un Array avec un seul nombre. Si il a
     * plusieurs solutions, la méthode retourne un Array avec plusieurs valeurs
     * @param value l'antécédent duquel on veut trouver l'image
     * @return le(s) nombre(s) solution
     */
    double[] findImages(double value);

}
