package fr.badcookie20.utils.math;

/**
 * Classe représentant un vecteur sur un plan en deux dimensions
 * @author BadCookie20
 */
public class Vector {

    private Position coordinates;
    /**
     * Constructeur du vecteur
     * @param start les coordonnées de départ du vecteur
     * @param end les coordonnées de fin du vecteur
     */
    public Vector(Position start, Position end) {
        this.coordinates = new Position(end.getX() - start.getX(), end.getY() - start.getY());
    }

    public Vector(Position coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * <p>Permet de récupérer les coordonnées du vecteur. Calculées selon la méthode qui suit :</p>
     * <li>soient a les coordonnées de départ du vecteur</li>
     * <li>soient b les coordonnées de fin du vecteur</li>
     * <li>résultat : X = Xb - Xa et Y = Yb - Ya</li>
     * @return les coordonnées du vecteur
     */
    public Position getCoordinates() {
        return coordinates;
    }

    /**
     * <p>Permet de savoir si ce vecteur est colinéaire au vecteur spécifié, grâce au calcul suivant :</p>
     * <li>soit a ce vecteur et b le vecteur spécifié</li>
     * <li>si Xa * Yb - Xb * Ya = 0 alors cette méthode retourne <code>true</code>, sinon <code>false</code></li>
     * @param v le vecteur à comparer avec celui-ci
     * @return <code>true</code> si les vecteur sont colinéaires, <code>false</code> si non
     */
    public boolean isColinear(Vector v) {
        Position p1 = this.getCoordinates();
        Position p2 = v.getCoordinates();
        return ((p1.getX() * p2.getY()) - (p2.getX() * p1.getY()) == 0);
    }

    /**
     * Permet de récupérer un vecteur qui vaut celui-ci multiplié par la valeur spécifiée.
     * @param value la valeur par laquelle multiplier ce vecteur (tout réel possible)
     * @return un vecteur correspondant à celui-ci multiplié par la valeur spécifiée
     */
    public Vector multiply(double value) {
        return new Vector(new Position(this.coordinates.getX() * value, this.coordinates.getX() * value));
    }

}
