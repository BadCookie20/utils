package fr.badcookie20.utils.math;

public class MathUtils {

    public static int getGreater(int... numbers) {
        int greater = Integer.MIN_VALUE;
        for(int i : numbers) {
            if(greater == Integer.MIN_VALUE) {
                greater = i;
                continue;
            }
            if(greater < i) greater = i;
        }

        return greater;
    }

    public static double getLenght(Position a, Position b) {
        return Math.sqrt(Math.pow(a.getX() - b.getX(), 2) + Math.pow(a.getY() - b.getX(), 2));
    }

    public static Position getCenter(Position a, Position b) {
        return new Position((a.getX() + b.getX()) /2, (a.getY() + b.getY()) / 2);
    }

    public static double getVectorLength(Vector v) {
        return getLenght(new Position(0, 0), v.getCoordinates());
    }

}
